CREATE DATABASE werkpakket1;
USE werkpakket1;
CREATE TABLE persons(
	personId INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	achternaam VARCHAR(25) NOT NULL,
	voornaam VARCHAR(25) NOT NULL,
	postcode INT(4) NOT NULL,
	gemeente VARCHAR(50) NOT NULL,
	straat VARCHAR(50) NOT NULL,
	huisnummer VARCHAR(5) NOT NULL,
	telefoonnummer INT(15),
	gsmnummer INT(15) NOT NULL,
	email VARCHAR(75) NOT NULL
);
CREATE TABLE events(
    eventId INT(10) NOT NULL,
	naam VARCHAR(25) NOT NULL,
	beginDatum DATE NOT NULL,
	eindDatum DATE NOT NULL,
	personId INT(10) NOT NULL,
	PRIMARY KEY (eventId),
	FOREIGN KEY (personId) REFERENCES persons(personId)
);

INSERT INTO persons VALUES(1, "Debrier", "Niels" ,  3511, "Kuringen", "vettersstraat" , "12" , NULL , 0477865432, "niels_debrier@hotmail.com" );
INSERT INTO persons VALUES(2, "Versteegen", "Merlijn" ,  3500, "Diepenbeek", "agoralaan" , "12" , NULL , 0477830267, "merlijn.versteegen@student.pxl.be" );
INSERT INTO persons VALUES(3, "Dominguez", "Kevin" , 3530, "Houthalen", "stationstraat" , "12" , NULL , 0477253849, "gekkespanjaard@telenet.be");
INSERT INTO klanten VALUES(4, "ElOuakili", "Hamza" ,  6700, "Beringen", "kerkstraat" , "12" , NULL , 0467294637, "Hamza@hotmail.com");
INSERT INTO events VALUES(1, "project1", '2017-01-01','2018-01-01',1);
INSERT INTO events VALUES(2, "project2", '2017-02-02','2018-02-02',2);
INSERT INTO events VALUES(3, "project1", '2017-03-03','2018-03-03',3);
INSERT INTO events VALUES(4, "project1", '2017-04-04','2018-04-04',4);

