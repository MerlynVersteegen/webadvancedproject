﻿
var results= [];

//dit is gewoon de wiskundige berekening om de fibonacci reeks te berekenen
function calculateNextFibonacciValue(n) {
    var s = 0;
    var returnValue;

    if (n == 0) {
        return (s);
    }
    if (n == 1) {
        s += 1;
        return (s);
    }
    else {
        return (calculateNextFibonacciValue(n - 1) + calculateNextFibonacciValue(n - 2));
    }
}

//deze functie genereert de fibbonacci reeks met de lengte n die wordt meegegeven.
//deze functie maakt gebruik van de functie calculatenextfibonaccivalue om het overzichtelijk te houden
function generateFibonacciSeries(n) {
    //stop 1 voor 1 alle waardes in results
    for (var i = 0; i <= n - 1; i++) {
        results.push(calculateNextFibonacciValue(i)); 
    }
    //deze functie stuurt de waarde van results door
    postMessage(results);
}