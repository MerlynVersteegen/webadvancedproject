﻿//dit is een werker die gewoon het script van fib-library importeert
//als vervolgens het ingegeven getal groter is dan 0 dan genereert die de fibonacci reeks
function messageHandler(e) {
    importScripts("fib-library.js");
    if (e.data > 0) {
        generateFibonacciSeries(e.data);
    }
}

//dit is de eventlistener die de functie messagehandler oproept wanneer er een message wordt getriggerd.
//In de fib-worker.htm --> postMessage
addEventListener("message", messageHandler, true);