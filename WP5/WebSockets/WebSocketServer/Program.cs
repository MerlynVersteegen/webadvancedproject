﻿using System;

namespace WebSocketServer
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine();
            Console.WriteLine("Welcome to the socket server.");
            Console.WriteLine();
            Console.WriteLine("Type \"connect\" to make a websocket connection ");
            Console.WriteLine();

            string input = Console.ReadLine();

            if (input == "connect")
            {
                HelloServer server = new HelloServer();
                server.Start();
            }
            else
            {
                Console.WriteLine(string.Format("Sorry, {0} is not a valid option.", input));
            }
        }
    }
}
