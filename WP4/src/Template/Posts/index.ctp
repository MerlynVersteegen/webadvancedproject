<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 *
 * Consulted Code: https://github.com/bradtraversy/mylogin/tree/master/src
 *
 * Modified by Hamza El Ouakili
 */

?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading">Dashboard</li>
        <li><?= $this->Html->link(__('Nieuwe Offerte'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Overzicht Personeel'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="posts index large-9 medium-8 columns content">
    <h3>Overzicht Offertes</h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Titel') ?></th>
                <th><?= $this->Paginator->sort('Verantwoordelijke') ?></th>
                <th><?= $this->Paginator->sort('Aangemaakt') ?></th>
                <th><?= $this->Paginator->sort('Aangepast') ?></th>
                <th class="actions"><?= __('Beheer') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post): ?>
            <tr>
                <td><?= h($post->title) ?></td>
                <td><?= $post->has('user') ? $this->Html->link($post->user->name, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?></td>
                <td><?= h($post->created) ?></td>
                <td><?= h($post->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Bekijk'), ['action' => 'view', $post->id]) ?>
                    <?= $this->Html->link(__('Bewerk'), ['action' => 'edit', $post->id]) ?>
                    <?= $this->Form->postLink(__('Verwijder'), ['action' => 'delete', $post->id], ['confirm' => __('Ben je zeker dat je offerte # {0} wil verwijderen?', $post->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Vorige')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Volgende') . ' >') ?>
        </ul>
    </div>
</div>