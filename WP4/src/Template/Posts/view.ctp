<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 *
 * Consulted Code: https://github.com/bradtraversy/mylogin/tree/master/src
 *
 * Modified by Hamza El Ouakili
 */

?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Dashboard') ?></li>
        <li><?= $this->Html->link(__('Bewerk'), ['action' => 'edit', $post->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Verwijder'), ['action' => 'delete', $post->id], ['confirm' => __('Ben je zeker dat je offerte # {0} wil verwijderen?', $post->id)]) ?> </li>
        <li><?= $this->Html->link(__('Overzicht Offertes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nieuwe Offerte'), ['action' => 'add']) ?> </li>

    </ul>
</nav>
<div class="posts view large-9 medium-8 columns content">
    <h3><?= h($post->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Titel') ?></th>
            <td><?= h($post->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Verantwoordelijke') ?></th>
            <td><?= $post->has('user') ? $this->Html->link($post->user->name, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Offerte ID') ?></th>
            <td><?= $this->Number->format($post->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Aangemaakt') ?></th>
            <td><?= h($post->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Aangepast') ?></th>
            <td><?= h($post->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Offerte Aanvraag') ?></h4>
        <?= $this->Text->autoParagraph(h($post->body)); ?>
    </div>
</div>