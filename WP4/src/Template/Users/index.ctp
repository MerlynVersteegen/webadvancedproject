<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 *
 * Consulted Code: https://github.com/bradtraversy/mylogin/tree/master/src
 *
 * Modified by Hamza El Ouakili
 */

?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Dashboard') ?></li>
        <li><?= $this->Html->link(__('Aanmaken Personeel'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Overzicht Offertes'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nieuwe Offerte'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Overzicht Personeel') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Personeels ID') ?></th>
                <th><?= $this->Paginator->sort('Naam') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('Aangemaakt') ?></th>
                <th><?= $this->Paginator->sort('Aangepast') ?></th>
                <th class="actions"><?= __('Beheer') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->name) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Bekijk'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__('Bewerk'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('Verwijder'), ['action' => 'delete', $user->id], ['confirm' => __('Ben je zeker dat je personeel met personeels id # {0} wil verwijderen?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Vorige')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Volgende') . ' >') ?>
        </ul>
    </div>
</div>