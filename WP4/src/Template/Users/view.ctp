<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 *
 * Consulted Code: https://github.com/bradtraversy/mylogin/tree/master/src
 *
 * Modified by Hamza El Ouakili
 */

?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Dashboard') ?></li>
        <li><?= $this->Html->link(__('Bewerk'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Verwijder'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('Overzicht Personeel'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Aanmaken Personeel'), ['action' => 'add']) ?> </li>

    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Naam') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Personeels ID') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Aangemaakt') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Aangepast') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Offertes in afwachting') ?></h4>
        <?php if (!empty($user->posts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Offerte ID') ?></th>
                <th><?= __('Titel') ?></th>
                <th><?= __('Aangemaakt') ?></th>
                <th><?= __('Aangepast') ?></th>
                <th class="actions"><?= __('Beheer') ?></th>
            </tr>
            <?php foreach ($user->posts as $posts): ?>
            <tr>
                <td><?= h($posts->id) ?></td>
                <td><?= h($posts->title) ?></td>
                <td><?= h($posts->created) ?></td>
                <td><?= h($posts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Bekijk'), ['controller' => 'Posts', 'action' => 'view', $posts->id]) ?>

                    <?= $this->Html->link(__('Bewerk'), ['controller' => 'Posts', 'action' => 'edit', $posts->id]) ?>

                    <?= $this->Form->postLink(__('Verwijder'), ['controller' => 'Posts', 'action' => 'delete', $posts->id], ['confirm' => __('Ben je zeker dat je personeel met personeels id  # {0} wil verwijderen?', $posts->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>