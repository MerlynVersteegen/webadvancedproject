<?php
/**
 * Created by PhpStorm.
 * User: Merlyn
 * Date: 19/04/2017
 * Time: 08:50
 */

namespace App\Controller;


class TransactionsController extends AppController {
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Flash'); // Include the FlashComponent
    }

    public function index()
    {
        //auto baked
        $transactions = $this->paginate($this->Transactions);
        //Get all transactions from model
        $transactions = $this->Transactions->find('all');
        //send data from controller to view
        $this->set(compact('transactions'));
    }
    public function view($id = null)
    {
        // Get transaction with that id
        $transaction = $this->Transactions->get($id);
        //send from controller to view
        $this->set('transaction', $transaction);
        // baked
        $this->set('_serialize', ['transaction']);
    }

    public function add()
    {
        $transaction = $this->Transactions->newEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        // Baked
        $this->set(compact('transaction'));
        $this->set('_serialize', ['transaction']);
    }

    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        // baked
        $this->set(compact('transaction'));
        $this->set('_serialize', ['transaction']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}