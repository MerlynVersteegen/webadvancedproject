<h1>Your Transaction history</h1>
<?= $this->Html->link('Add Transaction', ['action' => 'add']) ?>
<h2>Your current total: </h2>
<?php $totalamount = $transactions->sumOf('amount')?>
<h3> <?php echo $totalamount?></h3>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Amount</th>
        <th>Created</th>
    </tr>

    <!-- Getting all the data -->
    <?php foreach ($transactions as $transaction): ?>
        <tr>
            <td>
                <?= $transaction->id ?>
            </td>
            <td>
                <!-- generate link to the transaction -->
                <?= $this->Html->link($transaction->title, ['action' => 'view', $transaction->id]) ?>
            </td>
            <td>
                <?= $transaction->amount ?>
            </td>

            <td>
                <?= $transaction->created->format(DATE_RFC850) ?>
            </td>
            <td>
                <?= $this->Html->link('Edit', ['action' => 'edit', $transaction->id]) ?>
            </td>
            <td>
                <?= $this->Form->postLink(
                    'Delete',
                    ['action' => 'delete', $transaction->id],
                    ['confirm' => 'Are you sure?'])
                ?>

            </td>
        </tr>
    <?php endforeach; ?>
</table>