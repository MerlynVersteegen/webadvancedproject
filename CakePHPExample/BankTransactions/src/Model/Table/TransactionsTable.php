<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
/**
 * Created by PhpStorm.
 * User: Merlyn
 * Date: 19/04/2017
 * Time: 08:48
 */
class TransactionsTable extends Table{
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('transactions');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('title')
            ->requirePresence('title')
            ->notEmpty('amount')
            ->requirePresence('amount')
            ->decimal('amount',2,'please a number');
        return $validator;
    }

}